Experimental plugin to swap stages for custom Unity scenes in Blaze. See the HIVE: https://github.com/AndyLobjois/Custom-Stages/tree/main/Hive%20Underground

To swap a stage, place an AssetBundle with the same name as the stage (see Bundles/Stages folder for the names) inside the plugin's modding folder (in LLBlaze/ModdingFolder/Stage Swapper).

The AssetBundle should have a Unity scene. 
