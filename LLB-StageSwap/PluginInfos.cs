﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLB_StageSwap
{
    public static class PluginInfos
    {
        public const string PLUGIN_ID = "com.gitlab.axolotlll.llb-stageswap";
        public const string PLUGIN_NAME = "Stage Swapper";
        public const string PLUGIN_VERSION = "1.0.5";
    }
}
